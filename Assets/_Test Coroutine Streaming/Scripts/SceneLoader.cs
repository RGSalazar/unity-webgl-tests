using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    #region Inspector Fields

    [SerializeField]
    private GameObject uiLoading;

    #endregion //Inspector Fields

    #region Unity Callbacks

    private void Start()
    {
        uiLoading.SetActive(false);
    }

    #endregion //Unity Callbacks

    #region Public API

    public void LoadScene(string sceneName)
    {
        uiLoading.SetActive(true);
        SceneManager.LoadScene(sceneName);
    }

    public void LoadScene(int sceneIndex)
    {
        uiLoading.SetActive(true);
        SceneManager.LoadScene(sceneIndex);
    }

    #endregion //Public API

}
