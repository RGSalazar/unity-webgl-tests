using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class StreamingImageList : MonoBehaviour
{

    #region Inspector Fields

    [SerializeField]
    private List<string> paths;

    [SerializeField]
    private List<RawImage> imageDisplays;

    [SerializeField]
    private GameObject loadingUI;

    #endregion //Inspector Fields

    #region Unity Callbacks

    private void Start()
    {
        loadingUI.SetActive(false);
    }

    #endregion //Unity Callbacks

    #region Public API

    public void ClearPictures()
    {
        loadingUI.SetActive(false);

        foreach (var rawImage in imageDisplays)
        {
            rawImage.texture = null;
        }
    }

    public void LoadPictures()
    {
        ClearPictures();
        loadingUI.SetActive(true);
        StartCoroutine(StreamTextures());
    }

    #endregion //Public API

    #region Client Implementation

    /// <summary>
    /// Reference: https://docs.unity3d.com/Manual/UnityWebRequest-RetrievingTexture.html
    /// </summary>
    private IEnumerator StreamTextures()
    {
        Debug.Log($"{gameObject.name}.{GetType().Name}.GetTextures(): STARTED!", gameObject);

        var imageIndex = 0;

        foreach (var path in paths)
        {
            if (string.IsNullOrEmpty(path))
            {
                continue;
            }
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(path);
            yield return www.SendWebRequest();

            Texture myTexture = DownloadHandlerTexture.GetContent(www);
            Debug.Log($"{gameObject.name}.{GetType().Name}.GetTexture(): DONE! " +
                $"Texture is NULL? {myTexture == null}", gameObject);

            if (myTexture != null)
            {
                imageDisplays[imageIndex].texture = myTexture;
                imageDisplays[imageIndex].SetNativeSize();
                imageIndex++;
            }
        }

        loadingUI.SetActive(false);
    }

    #endregion //Client Implementation

}