using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class StreamImage : MonoBehaviour
{

    #region Inspector Fields

    [SerializeField]
    [Multiline]
    private string path;

    [SerializeField]
    private RawImage imageDisplay;

    [SerializeField]
    private GameObject loadingUI;

    #endregion //Inspector Fields

    #region Unity Callbacks

    private void Start()
    {
        loadingUI.SetActive(false);
    }

    #endregion //Unity Callbacks

    #region Public API

    public void ClearPicture()
    {
        loadingUI.SetActive(false);
        imageDisplay.texture = null;   
    }

    public void LoadPicture()
    {
        ClearPicture();
        loadingUI.SetActive(true);
        StartCoroutine(StreamTexture());
    }

    #endregion //Public API

    #region Client Implementation

    /// <summary>
    /// Reference: https://docs.unity3d.com/Manual/UnityWebRequest-RetrievingTexture.html
    /// </summary>
    private IEnumerator StreamTexture()
    {
        Debug.Log($"{gameObject.name}.{GetType().Name}.GetTexture(): STARTED!", gameObject);

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(path);
        yield return www.SendWebRequest();

        Texture myTexture = DownloadHandlerTexture.GetContent(www);
        Debug.Log($"{gameObject.name}.{GetType().Name}.GetTexture(): DONE! " +
            $"Texture is NULL? {myTexture == null}", gameObject);

        if (myTexture != null)
        {
            imageDisplay.texture = myTexture;
            imageDisplay.SetNativeSize();
        }

        loadingUI.SetActive(false);
    }

    #endregion //Client Implementation

}
