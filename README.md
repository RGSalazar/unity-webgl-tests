
# Image Streaming for WebGL

## _PROs_
- Images are straight from the internet
- It doesn't matter which site the images are hosted at. As long as they're downloadable/linkable, this workaround works (sample sites are flaticon.com and wallpaperflare.com)
- Memory and storage limit for a WebGL tab on the browser is not concerned when the images are being streamed
- Successfully streamed images can be cached _(if you turn on Data Caching on the project)_ With caching, it'll be faster to stream the next time.
- Caching is naturally implemented by any browser
- **No 3rd-party tool necessary for this workaround**
- Codes only rely on Unity's very own `UnityWebRequestTexture.GetTexture()` and `DownloadHandlerTexture.GetContent()` (reference link: https://docs.unity3d.com/Manual/UnityWebRequest-RetrievingTexture.html )
- No image compression to be done on the Unity side
- The image will be displayed as it is _(can resize the UI that renders the image, but that's it. So far.)_
- **This Image Streaming code also works on other platforms. Not just WebGL.** :)


## _CONs_
- Can only cater to internet images
- Streaming can be slow depending on 
	[1] the user's internet speed, 
	[2] the size of the image that is being streamed _(e.g. 4Ks are slower to stream than 512x512 images)_
- When the browser cache is full, the WebGL game will be slow. (Just as any other tab in the browser that loads new information). It is the responsibility of the Player to clear the cache and this is OUTSIDE of any developer's reach.
- NOT working with images from Google Drive. Google itself restricts getting the actual previewable/streamable image link from any user, and forces either plainly viewing on the web OR just downloading the images. Both of those options do not work with Unity's image streaming handlers.

# NOTE:
- You can see the image streaming feature on a WebGL project here: https://renslzr.itch.io/test-webgl-streaming-images
- I gotta check if there's any way to make streaming faster if all our images are 4Ks
	- An option is making the player wait (show him a loading screen) while we stream ALL images in the background so that after the streaming, their gameplay will be continuous
	- As an alternative, on our codes, we can do "image batching" to leverage the 4K streaming (we only stream an image when its turn to be displayed is projected to be near)
- I need to test more image streaming and see how 300+ of these can affect the gameplay.
	- Will the browser cache be full?
	- If the browser cache gets full in the middle of playing, will the game lag?
	- Will the game emit any funny errors or warnings? Anything possibly related to WebGL limits (e.g. Storage and Memory)? _(I personally wanna verify this)_